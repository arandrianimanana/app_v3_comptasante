import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import 'config/config.dart';
import 'src/components/custom_webview.dart';
import 'src/components/shortcut.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kCabName,
      theme: ThemeData(
        primarySwatch: kPrimary,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  final Tween<double> kDoubleTween = Tween<double>(begin: 0, end: 1);
  Animation<double> cardAnimation;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );

    cardAnimation = kDoubleTween.animate(
      CurvedAnimation(
        parent: controller,
        curve: const Interval(0, 0.1, curve: Curves.easeIn),
      ),
    );
    super.initState();
  }

  Future<bool> onBackPressed(BuildContext context) async {
    return await showPlatformDialog<bool>(
          context: context,
          builder: (context) => PlatformAlertDialog(
            content: Text('Quitter l\'application?'),
            actions: [
              PlatformDialogAction(
                child: PlatformText('Annuler'),
                onPressed: () => Navigator.pop(context, false),
              ),
              PlatformDialogAction(
                child: PlatformText('Quitter'),
                onPressed: () => Navigator.pop(context, true),
              )
            ],
            material: (_, __) => MaterialAlertDialogData(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 2,
            ),
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: WillPopScope(
        onWillPop: () => onBackPressed(context),
        child: Scaffold(
          body: Column(
            children: [
              const SizedBox(height: 32.0),
              Expanded(
                flex: 5,
                child: FadeTransition(
                  opacity: cardAnimation,
                  child: Center(
                    child: Image.asset(
                      'lib/config/customer/assets/logo_accueil.png',
                      frameBuilder: (
                        _,
                        child,
                        frame,
                        wasSynchronouslyLoaded,
                      ) {
                        if (frame != null) controller.forward();
                        return child;
                      },
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 32.0),
              Expanded(
                flex: 5,
                child: Wrap(
                  children: [
                    for (int i = 0; i < kRedirections.length; i++) ...[
                      FadeTransition(
                        opacity: kDoubleTween.animate(
                          CurvedAnimation(
                            parent: controller,
                            curve: Interval(
                              0.4 + i / 10,
                              0.5 + i / 10,
                            ),
                          ),
                        ),
                        child: Shortcut(
                          icon: Icon(kRedirections[i].icon,
                              color: kPrimary, size: 35.0),
                          text: kRedirections[i].title,
                          onTap: launchRedirection(context, kRedirections[i]),
                        ),
                      ),
                    ],
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
