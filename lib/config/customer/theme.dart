import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../src/models/office.dart';

const primaryColor = {
  50: Color.fromRGBO(134, 196, 236, .1),
  100: Color.fromRGBO(134, 196, 236, .2),
  200: Color.fromRGBO(134, 196, 236, .3),
  300: Color.fromRGBO(134, 196, 236, .4),
  400: Color.fromRGBO(134, 196, 236, .5),
  500: Color.fromRGBO(134, 196, 236, .6),
  600: Color.fromRGBO(134, 196, 236, .7),
  700: Color.fromRGBO(134, 196, 236, .8),
  800: Color.fromRGBO(134, 196, 236, .9),
  900: Color.fromRGBO(134, 196, 236, 1),
};

const secondaryColor = {
  50: Color.fromRGBO(134, 196, 236, .1),
  100: Color.fromRGBO(134, 196, 236, .2),
  200: Color.fromRGBO(134, 196, 236, .3),
  300: Color.fromRGBO(134, 196, 236, .4),
  400: Color.fromRGBO(134, 196, 236, .5),
  500: Color.fromRGBO(134, 196, 236, .6),
  600: Color.fromRGBO(134, 196, 236, .7),
  700: Color.fromRGBO(134, 196, 236, .8),
  800: Color.fromRGBO(134, 196, 236, .9),
  900: Color.fromRGBO(134, 196, 236, 1),
};

const kPrimaryHexa = 0xFF86c4ec;
const kPrimary = MaterialColor(kPrimaryHexa, primaryColor);
const kSecondary = MaterialColor(0xFF86c4ec, secondaryColor);

const kCabName = 'Comptasante';
const kCabEmail = 'contact@comptasante.fr';

const List<Redirection> kRedirections = [
  Redirection(
    title: 'Actualités',
    icon: FontAwesomeIcons.newspaper,
    type: RedirectionType.web,
    redirectionUrl: 'https://comptasante.fr/blog/',
    token: '',
    androidApp: '',
    iosApp: '',
  ),
  Redirection(
    title: 'Parrainage',
    icon: FontAwesomeIcons.gift,
    type: RedirectionType.web,
    redirectionUrl:
        'https://comptasante.fr/blog/offre-parrainage-6-mois-offerts-pour-le-filleul-2-mois-de-compta-rembourses-pour-le-parrain/',
    token: '',
    androidApp: '',
    iosApp: '',
  ),
  Redirection(
    title: 'Ma compta',
    icon: FontAwesomeIcons.chartLine,
    type: RedirectionType.app,
    redirectionUrl: 'isuiteexpert://',
    token: '',
    androidApp: 'fr.acd_groupe.isuiteexpertmobile11',
    iosApp: '1033556695',
  ),
];
