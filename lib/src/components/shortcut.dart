import 'package:flutter/material.dart';

class Shortcut extends StatelessWidget {
  Shortcut({
    @required this.icon,
    @required this.text,
    @required this.onTap,
  })  : assert(icon != null),
        assert(text != null),
        assert(onTap != null);

  final Widget icon;
  final VoidCallback onTap;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RawMaterialButton(
          onPressed: () => this.onTap(),
          child: this.icon,
          highlightElevation: 1,
          shape: const CircleBorder(),
          elevation: 1,
          fillColor: Colors.white,
          padding: const EdgeInsets.all(16.0),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 3,
          height: 64,
          child: Material(
            color: Colors.transparent,
            child: Text(
              this.text,
              softWrap: true,
              textAlign: TextAlign.center,
              overflow: TextOverflow.visible,
            ),
          ),
          padding: const EdgeInsets.all(12),
        ),
      ],
    );
  }
}
