import 'dart:io';

import 'package:device_apps/device_apps.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:launch_review/launch_review.dart';
import 'package:logger/logger.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/office.dart';

void Function() launchRedirection(BuildContext context, Redirection red) {
  return () {
    switch (red.type) {
      case RedirectionType.app:
        launchApp(red);

        break;
      default:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => WebViewScaffold(
              url: red.redirectionUrl,
              title: red.title,
            ),
          ),
        );
    }
  };
}

void launchApp(Redirection red) async {
  final androidPackageName = red.androidApp;
  final url = red.redirectionUrl;

  if (Platform.isAndroid && await DeviceApps.isAppInstalled(androidPackageName))
    await DeviceApps.openApp(androidPackageName);
  else {
    if (Platform.isIOS && await canLaunch(url)) {
      launch(url);
    } else {
      LaunchReview.launch(
        androidAppId: androidPackageName,
        iOSAppId: red.iosApp,
        writeReview: false,
      );
    }
  }
}

class WebViewScaffold extends StatelessWidget {
  WebViewScaffold({@required this.url, this.title});
  final title;
  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title ?? ''),
      ),
      body: CustomWebView(url: url),
    );
  }
}

class CustomWebView extends StatefulWidget {
  const CustomWebView(
      {@required this.url, this.enableLandscape = true, this.onCreated});
  final String url;
  final bool enableLandscape;
  final void Function(WebViewController) onCreated;

  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {
  int position;
  @override
  void initState() {
    position = 0;
    super.initState();
    if (widget.enableLandscape)
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
  }

  @override
  dispose() {
    if (widget.enableLandscape)
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: position,
      children: <Widget>[
        Center(
          child: const CircularProgressIndicator(),
        ),
        WebView(
          initialUrl: widget.url,
          javascriptMode: JavascriptMode.unrestricted,
          onPageStarted: (String url) => setState(() {
            position = 0;
          }),
          onPageFinished: (String url) => setState(() {
            position = 1;
          }),
          onWebViewCreated: (WebViewController webController) async {
            if (widget.onCreated != null) widget.onCreated(webController);
          },
          gestureRecognizers: {
            Factory<OneSequenceGestureRecognizer>(
              () => ScaleGestureRecognizer(),
            ),
          },
        ),
      ],
    );
  }
}
