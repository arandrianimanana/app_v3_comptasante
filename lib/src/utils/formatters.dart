extension StringExt on String {
  /// Converts first character in this string to upper case.
  ///
  /// ```dart
  /// 'alphabet'.upperFirst(); // 'Alphabet'
  /// ```
  String upperFirst() {
    return this.length < 2
        ? this.toUpperCase()
        : '${this[0].toUpperCase()}${this.substring(1)}';
  }

  bool get isNullOrEmpty {
    return this == null || this.isEmpty;
  }
}

extension ListExt on List {
  bool get isNullOrEmpty {
    return this == null || this.isEmpty;
  }
}
