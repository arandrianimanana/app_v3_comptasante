import 'package:flutter/material.dart';

import '../../config/config.dart';
import '../components/custom_webview.dart';
import '../utils/formatters.dart';

class Office {
  const Office({
    this.title,
    this.hours,
    this.email,
    this.phone,
    this.address1,
    this.address2,
    this.image,
  });

  final String address1;
  final String address2;
  final String email;
  final String hours;
  final String image;
  final String phone;
  final String title;
}

class Redirection {
  const Redirection({
    this.id,
    this.customIconPath,
    this.icon,
    this.redirectionUrl,
    this.token,
    this.title,
    this.type,
    this.androidApp,
    this.iosApp,
  });
  final int id;
  final String customIconPath;
  final IconData icon;
  final String redirectionUrl;
  final String token;
  final String androidApp;
  final String iosApp;
  final String title;
  final RedirectionType type;
}

enum RedirectionType {
  web,
  app,
  meg,
  megScan,
  isanet,
  isanetFact,
  isanetv2,
  netexcom,
  waibi,
  silae,
}

Redirection getRedirection(int id) =>
    kRedirections.firstWhere((red) => red.id == id, orElse: () => null);

Widget getRedirectionIcon(int id, {Color color, double size}) {
  if (size == null) size = IconThemeData.fallback().size;
  if (color == null) color = kPrimary;
  final red = getRedirection(id);
  return red.customIconPath.isNullOrEmpty
      ? Icon(red.icon, color: color, size: size)
      : Image(
          image: AssetImage(red.customIconPath),
          width: size,
          height: size,
        );
}

String getRedirectionTitle(int id) => getRedirection(id)?.title;

void Function() getRedirectionAction(BuildContext context, int id) =>
    launchRedirection(context, getRedirection(id));

extension RedirectionExt on int {
  /// returns true when value is > 100
  bool get isRedirection {
    return this > 100;
  }
}
